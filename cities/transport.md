## Общественный транспорт

### Новисад

1) Железка - [Srbijavoz](https://srbijavoz.rs).
2) Автобусы/etc - платить наличкой 65 динар.

### Белград

1) Железка - [Srbijavoz](https://srbijavoz.rs).
2) Автобусы/трамвай/etc. Оплата проезда и цены: [sms](beograd_bus.png) или через [mobile app](https://online.bgnaplata.rs/en/). В городе есть зоны проезда, в большинстве случае хватит A зоны, но лучше заранее погуглите этот вопрос.

## Такси

### Новисад

Yandex taxi/Red taxi/etc (в Google Play/Apple Store вбейте: **Novi Sad taxi**)

### Белград

Yandex taxi/Pink taxi/etc (в Google Play/Apple Store вбейте: **Belgrade taxi**)