## Mobile apps

1) [Moovit](https://moovitapp.com) - app для построения маршрутов общественного транспорта
2) [Srbijavoz](https://srbijavoz.rs) - app для покупки билетов на поезд
3) Mobile app вашего банка
4) Mobile app вашей sim (Yettel в моем случае)
5) [Blablacar](https://www.blablacar.rs) - блаблакар для Сербии
6) [KupujemProdajem](https://www.kupujemprodajem.com) - местный авито
7) Halo oglasi Nekretnine - mobile app [этого сайта](https://www.halooglasi.com/nekretnine)
8) [Glovo](https://glovoapp.com) - популярная доставка еды
9) [Wolt](https://wolt.com) - популярная доставка еды